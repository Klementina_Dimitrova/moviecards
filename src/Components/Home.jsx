import React, { useEffect, useState } from "react";
import { useContext } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import PostList from "./PostList";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import AccordionCmp from "./AccordionCmp";
import {
  Alert,
  AlertTitle,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

export default function Home() {
  const {
    deletePosts,
    handleKeyPress,
    isFilter,
    allPost,
    showFilter,
    setShowFilter,
    showFilterOnHome,
  } = useContext(GlobalContext);

  let history = useHistory();

  const mylist = JSON.parse(localStorage.getItem("idCardList"));

  const [openDialogList, setOpenDialogList] = useState(false);

  const handleClickOpenDialogList = () => {
    setOpenDialogList(true);
  };

  const handleCloseDialogList = () => {
    setOpenDialogList(false);
  };

  useEffect(() => {
    const post = localStorage.getItem("id");
    if (post) {
      history.push(`/details/${post}`);
    }
  }, []);
  

  return (
    <>
      <div className="homepage">
        <div className="header-homepage">
          <div className="title_list">
            <h1>IMDB TV Shows & Movies</h1>

            {mylist?.length !== 0 ? (
              <Link to={"/myList"}>
                <p>My List</p>
              </Link>
            ) : (
              <Link to={"/"} onClick={handleClickOpenDialogList}>
                <p>My List</p>
              </Link>
            )}
            <Dialog
              open={openDialogList}
              onClose={handleCloseDialogList}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                {"Add movies"}
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  <Typography variant="h6">Your list is currently empty.</Typography>
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDialogList} autoFocus>
                  Ok
                </Button>
              </DialogActions>
            </Dialog>
          </div>
          <TextField
            sx={{ backgroundColor: "white" }}
            placeholder="Search"
            onKeyDown={(e) => handleKeyPress(e)}
            variant="outlined"
            fullWidth
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton>
                    <SearchIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </div>
        <Grid
          container
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "baseline",
            flexDirection: "row",
            flexWrap: "wrap",
            marginTop: "1px",
            marginBottom: "20px",
          }}
          spacing={5}
        >
          <Grid item>
            {allPost && allPost.length >= 1 ? showFilterOnHome() : ""}

            {showFilter && showFilter === true ? <AccordionCmp /> : ""}
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              onClick={deletePosts}
              sx={{ marginTop: "20px" }}
              size="small"
            >
              Clear Page
            </Button>
          </Grid>
        </Grid>
        <PostList />
      </div>
    </>
  );
}
