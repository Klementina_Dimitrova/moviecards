import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import { GlobalContext } from "../Context/GlobalContext";

export default function AlertConfirmation({
  idPost,
  handleCloseConfirmation,
  openConfirmation,
}) {
  const { onDeletePost } = React.useContext(GlobalContext);

  return (
    <>
      <Dialog
        open={openConfirmation}
        onClose={handleCloseConfirmation}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete it?"}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleCloseConfirmation}>No</Button>
          <Button onClick={() => onDeletePost(idPost)} autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
