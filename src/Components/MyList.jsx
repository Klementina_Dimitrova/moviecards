import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import {
  Alert,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
} from "@mui/material";

export default function MyList() {
  const { showDetails, removePostFromList } = useContext(GlobalContext);

  const [myList, setMyList] = useState([]);

  const [openDialogList, setOpenDialogList] = useState(false);

  const handleClickOpenDialogList = () => {
    setOpenDialogList(true);
  };

  const handleCloseDialogList = () => {
    setOpenDialogList(false);
  };

  useEffect(() => {
    console.log("rerender!");
    const myListStorage = JSON.parse(localStorage.getItem("idCardList"));
    setMyList(myListStorage);
  }, [myList]);

  return (
    <>
      <div className="cardsList">
        {myList?.length >= 1 ? (
          <Grid
          container
          spacing={2}

          xs={11} sm={4}  md={6} lg={6}
          sx={{
            display: "flex",
            flexWrap: "wrap",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            marginTop: "10px",
          }}
        >
          {myList.map((item, idx) => (
                <Card
                  sx={{
                    height:'70vh',
                    maxWidth: 280,
                    flexWrap: "wrap",
                    margin: "10px",
                    borderRadius: "10px",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Link to={`/details/${item.id}`}>
                    <CardMedia
                      component="img"
                      alt="green iguana"
                      image={item.image}
                    />
                  </Link>
                  <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                      {item.title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      <Box> Genre: {item.genre}</Box>
                      <Box>IMDB Rating: {item.imDbRating}</Box>
                    </Typography>
                  </CardContent>
                  <CardActions
                    sx={{
                      display: "flex",
                      justifyContent: "center",
                      alignContent: "center",
                      marginBottom: "5px",
                      marginTop: "auto",
                    }}
                  >
                    <Button
                      variant="contained"
                      onClick={() => {
                        removePostFromList(item.id);
                      }}
                      style={{ marginLeft: "10px", textTransform: "none" }}
                      size="small"
                    >
                      Remove
                    </Button>
                  </CardActions>
                </Card>
          ))}
          </Grid>
        )  : (
          <Alert
            severity="warning"
            action={
              <Link
                to={"/"}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignSelf: "center",
                }}
              >
                OK
              </Link>
            }
            sx={{ padding: "40px", backgroundColor: "white" }}
          >
            Your list is currently empty.
          </Alert>
        )}
      </div>
    </>
  );
}
