import React, { useContext, useEffect, useState } from "react";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import { Alert, Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import { GlobalContext } from "../Context/GlobalContext";

export default function AccordionCmp() {
  const { allPost, setAllPost, selectedCheckbox, setSelectedCheckbox } =
    useContext(GlobalContext);

  const posts = allPost;

  const [postToShow, setPostToShow] = useState(posts);

  const filterGenre = (e) => {
    const query = e;

    if (query.includes(selectedCheckbox)) {
      setAllPost(postToShow);
    } else if (query !== selectedCheckbox) {
      const filtered = postToShow.filter((post) =>
        post.genre.toLowerCase().includes(query.toLowerCase())
      );

      if (filtered.length === 0) {
        {
          setAllPost(filtered);
          console.log("No movies found.");
        }
      } else if (filtered.length !== 0) {
        // console.log("Enter");
        setAllPost(filtered);
      }
    }
  };

  const handleCheckboxChange = (e) => {
    const queryValue = e;
    setSelectedCheckbox(queryValue === selectedCheckbox ? null : queryValue);
    filterGenre(queryValue);
  };

  return (
    <Accordion className="acc_cmp" sx={{ width: "270px" }}>
      <AccordionSummary
        expandIcon={<ArrowDownwardIcon />}
        aria-controls="panel1-content"
        id="panel1-header"
      >
        <Typography>Filter</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox
                  value="Sci-Fi"
                  checked={"Sci-Fi" === selectedCheckbox}
                  onChange={(e) => handleCheckboxChange(e.target.value)}
                />
              }
              label="Sci-Fi"
            />
            <FormControlLabel
              control={
                <Checkbox
                  value="Drama"
                  checked={"Drama" === selectedCheckbox}
                  onChange={(e) => handleCheckboxChange(e.target.value)}
                />
              }
              label="Drama"
            />
            <FormControlLabel
              control={
                <Checkbox
                  value="Action"
                  checked={"Action" === selectedCheckbox}
                  onChange={(e) => handleCheckboxChange(e.target.value)}
                />
              }
              label="Action"
            />
            <FormControlLabel
              control={
                <Checkbox
                  value="Fantasy"
                  checked={"Fantasy" === selectedCheckbox}
                  onChange={(e) => handleCheckboxChange(e.target.value)}
                />
              }
              label="Fantasy"
            />
          </FormGroup>
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
}
