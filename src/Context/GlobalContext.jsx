import React, { Children, createContext, useState, useEffect } from "react";
import data from "../data/database.json";

export const GlobalContext = createContext();
export const GlobalProvider = ({ children }) => {
  const [allPost, setAllPost] = useState();
  const [searchQuery, setSearchQuery] = useState();

  const [postDetails, setPostDetails] = useState();

  const [addToList, setAddToList] = useState([]);

  const [toggleRerender, setToggleRerender] = useState(false);

  const [showFilter, setShowFilter] = useState(false);

  const [selectedCheckbox, setSelectedCheckbox] = useState(null);

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      handlerOnSearch(event);
    }
  };

  const handlerOnSearch = (e) => {
    const query = e.target.value;
    const filtered = data.data.filter((post) =>
      post.title.toLowerCase().includes(query.toLowerCase())
    );
    setAllPost(filtered);
    setSearchQuery(query);
  };

  const onDeletePost = (id) => {
    const findPost = allPost.filter((post) => post.id !== id);
    setAllPost(findPost);
  };

  const deletePosts = () => {
    setAllPost([]);
    setShowFilter(false);
  };

  const showDetails = (id) => {
    const foundPost = data.data.find((post) => post.id === id);
    setPostDetails(foundPost);
  };

  const myList = (id) => {
    const foundPost = data.data.find((post) => post.id === id);
    setAddToList([...addToList, foundPost]);
    localStorage.setItem(
      "idCardList",
      JSON.stringify([...addToList, foundPost])
    );
  };

  const removePostFromList = (id) => {
    let posts = JSON.parse(localStorage.getItem("idCardList"));
    let newPosts = posts.filter((post) => post.id !== id);
    setAddToList(newPosts);
    localStorage.setItem("idCardList", JSON.stringify(newPosts));
    {
      console.log(newPosts);
    }
  };

  const appToFavorite = (id) => {
    if (toggleRerender === false) {
      const foundPost = data.data.find((post) => post.id === id);
      localStorage.setItem("id", foundPost.id);
      setToggleRerender((prev) => !prev);
    } else if (toggleRerender === true) {
      localStorage.removeItem("id");
      setToggleRerender((prev) => !prev);
    }
  };
  const removePost = () => {
    localStorage.removeItem("id");
  };
  const filterPost = (e) => {
    const query = e.target.value;
    const searchedPost = data.data.filter((post) =>
      post.title.toLowerCase().includes(searchQuery.toLowerCase())
    );
    const filteredPost = allPost.filter((post) =>
      post.title.toLowerCase().includes(query.toLowerCase())
    );

    if (query.length === 0) {
      setAllPost(searchedPost);
    } else {
      setAllPost(filteredPost);
    }
  };

  const showFilterOnHome = () => {
    if (allPost.length >= 1) {
      setShowFilter(true);
    }
  };

  const contexObj = {
    data,
    allPost,
    setAllPost,
    handlerOnSearch,
    handleKeyPress,
    deletePosts,
    showDetails,
    postDetails,
    appToFavorite,
    removePost,
    onDeletePost,
    filterPost,
    searchQuery,
    setSearchQuery,
    myList,
    addToList,
    removePostFromList,
    toggleRerender,
    setToggleRerender,
    selectedCheckbox,
    setSelectedCheckbox,
    showFilter,
    setShowFilter,
    showFilterOnHome,
  };

  return (
    <GlobalContext.Provider value={contexObj}>
      {children}
    </GlobalContext.Provider>
  );
};
