import './App.css';
import { GlobalProvider } from './Context/GlobalContext';
import Home from './Components/Home';
import Details from './Components/Details'
import { BrowserRouter as Router, Route, } from 'react-router-dom'
import MyList from './Components/MyList';

function App() {
  
  return (
    <div className="App">
      <GlobalProvider>
        <Router>
          <Route path={'/'} exact component={Home} />
          <Route path={'/details/:id'} component={Details} />
          <Route path={'/myList'} component={MyList}/>
        </Router>
      </GlobalProvider>
    </div>
  );
}

export default App;
