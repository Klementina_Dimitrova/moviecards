import React, { useContext, useEffect } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import Cards from "./Card";
import { Grid } from "@mui/material";

export default function PostList() {
  const { allPost, filterPost } = useContext(GlobalContext);

  return (
    <div className="post-list">
      {allPost == 0 ? (
        <div className="no_moviesFound">
          <p className="no_movies">No movies found</p>
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexWrap: "wrap",
            flexDirection: "row",
          }}
        >
          <Grid
            container
            spacing={2}
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
              alignContent: "center",
            }}
          >
            {allPost?.map((post) => {
              return (
                <Grid
                  item
                  xs={12}
                  sm={8}
                  lg={3}
                  sx={{ display: "flex", flex: 1 }}
                >
                  <Cards key={post.id} data={post} />
                </Grid>
              );
            })}
          </Grid>
        </div>
      )}
    </div>
  );
}