import React, { useContext } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import { Link } from "react-router-dom";
import {
  Box,
  ButtonBase,
  Grid,
  ThemeProvider,
  Typography,
  createTheme,
} from "@mui/material";

export default function Details(props) {
  const { data, removePost } = useContext(GlobalContext);

  const foundPost = data.data.find((post) => post.id === props.match.params.id);

  const theme = createTheme();

  theme.typography.h1 = {
    display: "flex",
    "@media (min-width:320px)": {
      fontSize: "1rem",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
      marginTop: "20px",
      marginBottom: "30px",
    },
  };
  theme.typography.p = {
    display: "flex",
    marginTop: "10px",
    "@media (min-width:320px)": {
      fontSize: "0.8rem",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "1.05rem",
    },
    textAlign: "left",
    color: "rgb(93, 170, 196)",
  };
  theme.typography.p1 = {
    color: "wheat",
    marginTop: "10px",
    "@media (min-width:320px)": {
      fontSize: "1.2rem",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "2rem",
    },
  };

  return (
    <Grid
      container
      spacing={2}
      sx={{
        backgroundPosition: "center",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        minHeight: "100vh",
        justifyContent: "flex-start",
        marginTop: "0px",
      }}
      direction={"column"}
      className="details"
    >
      <Grid item>
        <Link to={"/"} onClick={removePost}>
          <ThemeProvider theme={theme}>
            <Typography variant="p1">Back to search result</Typography>
          </ThemeProvider>
        </Link>
      </Grid>
      <Grid
        item
        sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}
      >
        <Grid
          item
          sx={{
            borderRadius: "5px",
            margin: "0px auto",
            width: { sm: "40%", xs: "80%" },
            backgroundColor: "white",
            display: "flex",
            flexDirection: { xs: "column", sm: "row" },
            justifyContent: "center",
          }}
        >
          <Grid item lg={6} xs={12}>
            <ButtonBase>
              <Box
                component="img"
                sx={{
                  mb: 4,
                  marginBottom: "20px",
                  height: { sm: "330px", xs: "240px" },
                  marginTop: "20px",
                }}
                src={foundPost.image}
              />
            </ButtonBase>
          </Grid>
          <Grid
            item
            lg={6}
            xs={12}
            sx={{ marginLeft: "25px", marginBottom: "20px" }}
          >
            <ThemeProvider theme={theme}>
              <Typography variant="h1" display={"flex"}>
                {foundPost.title}
              </Typography>
            </ThemeProvider>
            <ThemeProvider theme={theme}>
              <Typography variant="p">Crew:{foundPost.crew}</Typography>
            </ThemeProvider>
            <ThemeProvider theme={theme}>
              <Typography variant="p">IMDB:{foundPost.imDbRating}</Typography>
            </ThemeProvider>
            <ThemeProvider theme={theme}>
              <Typography variant="p">
                Rating count:{foundPost.imDbRatingCount}
              </Typography>
            </ThemeProvider>
            <ThemeProvider theme={theme}>
              <Typography variant="p">Rank:{foundPost.rank}</Typography>
            </ThemeProvider>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}