import React, { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../Context/GlobalContext";
import AlertConfirmation from "./AlertConfirmation";
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { red } from "@mui/material/colors";

export default function Cards({ data }) {
  const {
    showDetails,
    appToFavorite,
    myList,
    toggleRerender,
    setToggleRerender,
  } = useContext(GlobalContext);
  const { id, title, image, imDbRating, genre } = data;

  const List = JSON.parse(localStorage.getItem("idCardList"));
  const filteredList = List?.filter((item) => item.id === id);
  const movieListId = filteredList?.[0]?.id;

  useEffect(() => {}, [toggleRerender]);

  const currentFavoriteMovieId = localStorage.getItem("id");

  const [openConfirmation, setOpenConfirmation] = useState(false);

  const handleClickOpenConfirmation = () => {
    setOpenConfirmation(true);
  };

  const handleCloseConfirmation = () => {
    setOpenConfirmation(false);
  };

  return (
    <Card
      sx={{
        width: "100%",
        margin: "10px",
        borderRadius: "10px",
        position: "relative",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
      }}
    >
      <Button
        sx={{
          position: "absolute",
          right: "0px",
          top: "5px",
          color: "red",
          fontSize: "16.5px",
        }}
        onClick={handleClickOpenConfirmation}
      >
        <b>X</b>
      </Button>
      {openConfirmation && (
        <AlertConfirmation
          openConfirmation={openConfirmation}
          handleCloseConfirmation={handleCloseConfirmation}
          idPost={id}
        />
      )}
      <Link to={`/details/${id}`}>
        <CardMedia component="img" alt="green iguana" image={image} />
      </Link>
      <CardContent>
        <Typography gutterBottom variant="h6" component="div">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          <Box> Genre: {genre}</Box>
          <Box>IMDB Rating: {imDbRating}</Box>
        </Typography>
      </CardContent>
      <CardActions
        sx={{
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          marginBottom: "5px",
          marginTop: "auto",
        }}
      >
        <Button
          variant="contained"
          disabled={currentFavoriteMovieId && currentFavoriteMovieId !== id}
          className={
            currentFavoriteMovieId === id ? "btn-enabled" : "btn-disabled"
          }
          onClick={() => appToFavorite(id)}
          style={{ marginLeft: "10px", textTransform: "none" }}
          size="small"
        >
          {currentFavoriteMovieId && currentFavoriteMovieId === id
            ? "Saved"
            : "Favorite"}
        </Button>
        {movieListId !== id && (
          <Button
            variant="contained"
            onClick={() => myList(id)}
            style={{ textTransform: "none" }}
            size="small"
          >
            Add to List
          </Button>
        )}
      </CardActions>
    </Card>
  );
}
